package cowsimulator.version4

class CowMain {

    static main(args) {
        Cow zwartbont = new Zwartbont()
        Cow roodbont = new Roodbont()
        Cow geneticSuperCow = new GeneticSuperCow()
        
        zwartbont.printCowType()
        zwartbont.moo()
        zwartbont.eat()
        
        roodbont.printCowType()
        roodbont.moo()
        roodbont.eat()
        
        geneticSuperCow.printCowType()
        geneticSuperCow.moo()
        geneticSuperCow.eat()
    }

}
