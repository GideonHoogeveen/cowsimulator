package cowsimulator.version4

class GeneticSuperCow extends Cow {
    
    public void moo() {
        println 'moehoe'
    }
    
    public void eat() {
        println '...'
    }
    
    public void printCowType() {
        println 'genetic super cow'
    }
}
