package cowsimulator.version4

abstract class Cow {
    
    public void moo() {
        println 'boe'
    }
    public void eat() {
        println 'eating'
    }
    
    public void printCowType() {
        println 'just a normal cow'
    }
}
