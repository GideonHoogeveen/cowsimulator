package cowsimulator.version7.cow.mooing

class NoMooing implements Mooing{

    @Override
    public void moo() {
        println '...'
    }

}
