package cowsimulator.version7.cow

import cowsimulator.version7.cow.eating.Eating;
import cowsimulator.version7.cow.mooing.Mooing;
import cowsimulator.version7.manger.Manger;


class Cow {
    
    CowType cowType
    Eating eatingBehaviour
    Mooing mooingBehaviour
    
    public void moo() {
        mooingBehaviour.moo()
    }
    
    public void eat(Manger manger) {
        eatingBehaviour.eat(manger)
    }
    
    public void printCowType() {
        println cowType
    }
    
}
