package cowsimulator.version7.cow.eating

import cowsimulator.version7.manger.Manger;

class SlowEating implements Eating{

    @Override
    public void eat(Manger manger) {
        println 'eating 1 meal'
        manger.takeFood(1)
    }

}
