package cowsimulator.version7.cow.eating

import cowsimulator.version7.manger.Manger;

class FastEating implements Eating{

    @Override
    public void eat(Manger manger) {
        println 'eating 2 meals'
        manger.takeFood(2)
    }

}
