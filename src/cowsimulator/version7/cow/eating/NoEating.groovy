package cowsimulator.version7.cow.eating

import cowsimulator.version7.manger.Manger;

class NoEating implements Eating{

    @Override
    public void eat(Manger manger) {        println 'Not eating any meal'
    }

}
