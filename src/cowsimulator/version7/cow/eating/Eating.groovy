package cowsimulator.version7.cow.eating

import cowsimulator.version7.manger.Manger;

interface Eating {
    public void eat(Manger manger)
}
