package cowsimulator.version7.manger

class Manger {
    
    private int meals
    
    public void addFood(int amount) {
        meals += amount
    }
    
    public void takeFood(int amount) {
        meals -= amount
    }
    
    public void foodLeft() {
        println "food left: $meals"
    }
}
