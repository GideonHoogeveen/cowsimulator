package cowsimulator.version5

class SlowEating implements Eating{

    @Override
    public void eat() {
        println 'eating slowly'
    }

}
