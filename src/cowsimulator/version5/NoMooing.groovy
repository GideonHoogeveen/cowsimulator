package cowsimulator.version5

class NoMooing implements Mooing{

    @Override
    public void moo() {
        println '...'
    }

}
