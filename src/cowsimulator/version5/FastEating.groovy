package cowsimulator.version5

class FastEating implements Eating{

    @Override
    public void eat() {
        println 'eating fast'
    }

}
