package cowsimulator.version5


class CowMain {

    static main(args) {
        Cow zwartbont = new Cow(cowType: CowType.ZWARTBONT, eatingBehaviour: new FastEating(), mooingBehaviour: new NoMooing())
        Cow roodbont = new Cow(cowType: CowType.ROODBONT, eatingBehaviour: new SlowEating(), mooingBehaviour: new LoudMooing())
        Cow geneticSuperCow = new Cow(cowType: CowType.GENETIC_SUPER_COW, eatingBehaviour: new NoEating(), mooingBehaviour: new LoudMooing())
        
        println 'fast eating silent cow:'
        zwartbont.printCowType()
        zwartbont.eat()
        zwartbont.moo()
        
        println 'slow eating loud cow:'
        roodbont.printCowType()
        roodbont.eat()
        roodbont.moo()
        
        println 'no eating loud cow:'
        geneticSuperCow.printCowType()
        geneticSuperCow.eat()
        geneticSuperCow.moo()
    }

}
