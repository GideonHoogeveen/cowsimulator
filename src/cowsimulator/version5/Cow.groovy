package cowsimulator.version5


class Cow {
    
    CowType cowType
    Eating eatingBehaviour
    Mooing mooingBehaviour
    
    public void moo() {
        mooingBehaviour.moo()
    }
    
    public void eat() {
        eatingBehaviour.eat()
    }
    
    public void printCowType() {
        println cowType
    }
    
}
