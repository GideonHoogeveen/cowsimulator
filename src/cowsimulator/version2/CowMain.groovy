package cowsimulator.version2

class CowMain {

    static main(args) {
        Cow zwartbont = new Cow('ZB')
        Cow roodbont = new Cow('RB')
        
        println 'zwartbont:'
        zwartbont.moo()
        zwartbont.eat()
        
        println 'roodbont:'
        roodbont.moo()
        roodbont.eat()
    }

}
