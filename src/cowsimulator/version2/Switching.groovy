package cowsimulator.version2

class Switching {

    static main(args) {
        String zwartbont = 'ZB'
        String roodbont = 'RB'
        
        println 'zwartbont:'
        moo(zwartbont)
        eat(zwartbont)
        
        println 'roodbont:'
        moo(roodbont)
        eat(roodbont)
    }
    
    static void moo(String cowType) {
        switch(cowType) {
            case 'ZB':
                println 'moe'
                break;
            case 'RB':
                println 'boe'
                break;
        }
            
    }
    
    static void eat(String cowType) {
        println 'eating'
    }

}
