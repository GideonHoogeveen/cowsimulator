package cowsimulator.version2

class Cow {
    String cowType
    
    public Cow(String cowType) {
        this.cowType = cowType
    }
    
    public void moo() {
        switch(cowType) {
            case 'ZB':
                println 'moe'
                break;
            case 'RB':
                println 'boe'
                break;
        }
    }
    
    public void eat() {
        println 'eating'
    }
}
