package cowsimulator.version3

class Cow {
    String cowType
    
    public Cow(String cowType) {
        this.cowType = cowType
    }
    
    public void moo() {
        switch(cowType) {
            case 'ZB':
                println 'moe'
                break;
            case 'RB':
                println 'boe'
                break;
            case 'GSC':
                println 'boehoe'
                break
        }
    }
    
    public void eat() {
        if(cowType == 'GSC') {
            println '...'
        } else {
            println 'eating'
        }
    }
    
    public void printCowType() {
        println cowType
    }
}
