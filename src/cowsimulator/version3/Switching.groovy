package cowsimulator.version3

class Switching {

    static main(args) {
        String zwartbont = 'ZB'
        String roodbont = 'RB'
        String standbeeld = 'standbeeld'
        
        printCowType(zwartbont)
        moo(zwartbont)
        eat(zwartbont)
        
        printCowType(roodbont)
        moo(roodbont)
        eat(roodbont)
        
        printCowType(standbeeld)
        moo(standbeeld)
        eat(standbeeld)
    }
    
    static void moo(String cowType) {
        switch(cowType) {
            case 'ZB':
                println 'moe'
                break;
            case 'RB':
                println 'boe'
                break;
            case 'standbeeld':
                println 'boehoe'
                break
        }
            
    }
    
    static void eat(String cowType) {
        if(cowType == 'standbeeld') {
            println '...'
        } else {
            println 'eating'
        }
    }
    
    static void printCowType(String cowType) {
        println cowType
    }

}
