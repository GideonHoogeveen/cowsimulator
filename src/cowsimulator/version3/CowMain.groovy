package cowsimulator.version3

class CowMain {

    static main(args) {
        Cow zwartbont = new Cow('ZB')
        Cow roodbont = new Cow('RB')
        Cow geneticSuperCow = new Cow('GSC')
        
        zwartbont.printCowType()
        zwartbont.moo()
        zwartbont.eat()
        
        roodbont.printCowType()
        roodbont.moo()
        roodbont.eat()
        
        geneticSuperCow.printCowType()
        geneticSuperCow.moo()
        geneticSuperCow.eat()
    }

}
