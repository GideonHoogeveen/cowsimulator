package cowsimulator.version8.cow

import cowsimulator.version7.cow.eating.Eating;
import cowsimulator.version7.cow.mooing.Mooing;
import cowsimulator.version7.manger.Manger;
import cowsimulator.version8.manger.FoodContainer;


class Cow {
    
    CowType cowType
    Eating eatingBehaviour
    Mooing mooingBehaviour
    
    public void moo() {
        mooingBehaviour.moo()
    }
    
    public void eat(FoodContainer foodContainer) {
        eatingBehaviour.eat(foodContainer)
    }
    
    public void printCowType() {
        println cowType
    }
    
}
