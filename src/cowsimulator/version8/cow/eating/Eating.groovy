package cowsimulator.version8.cow.eating

import cowsimulator.version8.manger.FoodContainer

interface Eating {
    public void eat(FoodContainer foodContainer)
}
