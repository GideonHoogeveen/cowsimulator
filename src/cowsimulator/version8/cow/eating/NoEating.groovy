package cowsimulator.version8.cow.eating

import cowsimulator.version7.manger.Manger;
import cowsimulator.version8.manger.FoodContainer;

class NoEating implements Eating{

    @Override
    public void eat(FoodContainer foodContainer) {        println 'Not eating any meal'
    }

}
