package cowsimulator.version8.cow.eating

import cowsimulator.version7.manger.Manger;
import cowsimulator.version8.manger.FoodContainer;

class FastEating implements Eating{

    @Override
    public void eat(FoodContainer foodContainer) {
        println 'eating 2 meals'
        foodContainer.takeFood(2)
    }

}
