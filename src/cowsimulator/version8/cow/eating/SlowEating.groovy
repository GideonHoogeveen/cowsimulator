package cowsimulator.version8.cow.eating

import cowsimulator.version7.manger.Manger;
import cowsimulator.version8.manger.FoodContainer;

class SlowEating implements Eating{

    @Override
    public void eat(FoodContainer foodContainer) {
        println 'eating 1 meal'
        foodContainer.takeFood(1)
    }

}
