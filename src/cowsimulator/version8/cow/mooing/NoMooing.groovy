package cowsimulator.version8.cow.mooing

class NoMooing implements Mooing{

    @Override
    public void moo() {
        println '...'
    }

}
