package cowsimulator.version8.manger

class Manger implements FoodContainer{
    
    private int meals
    
    public void addFood(int amount) {
        meals += amount
    }
    
    public void takeFood(int amount) {
        meals -= amount
    }
    
    public int foodLeft() {
        return meals
    }
}
