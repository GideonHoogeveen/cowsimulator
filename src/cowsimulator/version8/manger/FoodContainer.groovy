package cowsimulator.version8.manger

interface FoodContainer {
    public void addFood(int amount)
    public void takeFood(int amount)
    public int foodLeft()
}
