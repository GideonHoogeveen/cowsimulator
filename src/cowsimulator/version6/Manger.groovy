package cowsimulator.version6

class Manger {
    
    private int meals
    
    public void addFood(int amount) {
        meals += amount
    }
    
    public void takeFood(int amount) {
        meals -= amount
    }
    
    public void foodLeft() {
        println "food left: $meals"
    }
}
