package cowsimulator.version6

class SlowEating implements Eating{

    @Override
    public void eat(Manger manger) {
        println 'eating 1 meal'
        manger.takeFood(1)
    }

}
