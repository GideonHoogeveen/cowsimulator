package cowsimulator.version6

class NoMooing implements Mooing{

    @Override
    public void moo() {
        println '...'
    }

}
