package cowsimulator.version6


class CowMain {

    static main(args) {
        Cow zwartbont = new Cow(cowType: CowType.ZWARTBONT, eatingBehaviour: new FastEating(), mooingBehaviour: new NoMooing())
        Cow roodbont = new Cow(cowType: CowType.ROODBONT, eatingBehaviour: new SlowEating(), mooingBehaviour: new LoudMooing())
        Cow geneticSuperCow = new Cow(cowType: CowType.GENETIC_SUPER_COW, eatingBehaviour: new NoEating(), mooingBehaviour: new LoudMooing())
        
        Manger manger = new Manger()
        manger.addFood(5)
        
        println '--------------------------'
        println 'fast eating silent cow:'
        zwartbont.printCowType()
        manger.foodLeft()
        zwartbont.eat(manger)
        manger.foodLeft()
        zwartbont.moo()
        
        println '--------------------------'
        println 'slow eating loud cow:'
        roodbont.printCowType()
        manger.foodLeft()
        roodbont.eat(manger)
        manger.foodLeft()
        roodbont.moo()
        
        println '--------------------------'
        println 'no eating loud cow:'
        geneticSuperCow.printCowType()
        manger.foodLeft()
        geneticSuperCow.eat(manger)
        manger.foodLeft()
        geneticSuperCow.moo()
    }

}
