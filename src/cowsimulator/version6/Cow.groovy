package cowsimulator.version6


class Cow {
    
    CowType cowType
    Eating eatingBehaviour
    Mooing mooingBehaviour
    
    public void moo() {
        mooingBehaviour.moo()
    }
    
    public void eat(Manger manger) {
        eatingBehaviour.eat(manger)
    }
    
    public void printCowType() {
        println cowType
    }
    
}
