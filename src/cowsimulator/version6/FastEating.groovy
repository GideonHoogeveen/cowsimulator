package cowsimulator.version6

class FastEating implements Eating{

    @Override
    public void eat(Manger manger) {
        println 'eating 2 meals'
        manger.takeFood(2)
    }

}
